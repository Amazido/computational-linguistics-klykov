# coding=utf-8
from lxml import etree
import os
import re
import io

def fileToXml(page, f):
    currentSentence = ""
    while True:
        buf = f.read(1)
        if not buf: break
        if buf == "\n" or buf == "?" or buf == "!" or buf == ".":
            if currentSentence == "":
                continue
            currentTagXml = etree.SubElement(page, 'sent')
            currentSentence = re.sub(r"[$«»()\.,%/\-—–:;\n]", '', currentSentence)
            print (currentSentence)
            currentTagXml.text = currentSentence
            currentSentence = ""
        else:
            currentSentence += buf

    if currentSentence != "":
        currentTagXml = etree.SubElement(page, 'sent')
        currentSentence = re.sub(r"\W", '', currentSentence)
        currentTagXml.text = currentSentence
    return


def textFilesToXmlFile(directory, files):
    page = etree.Element('document')
    for file in files:
        print(file)
        file = io.open(directory + file, encoding='windows-1251')
        fileToXml(page, file)
        output=io.open(directory + 'output.xml', 'w', encoding='utf-8')
        output.write(etree.tounicode(page, pretty_print=True))


directory = 'science/'
if os.path.isfile(directory+'output.xml'):
    os.remove(directory+'output.xml')
files = os.listdir(directory)
textFilesToXmlFile(directory, files)

directory = 'space/'
if os.path.isfile(directory+'output.xml'):
    os.remove(directory+'output.xml')
files = os.listdir(directory)
textFilesToXmlFile(directory, files)