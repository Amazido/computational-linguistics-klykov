# coding=utf-8
import operator

import IndexBuilder
from scipy import spatial, math
from collections import Counter

def compute_tfidf(corpus):
    def compute_tf(text):
        tf_text = Counter(text)
        for i in tf_text:
            tf_text[i] = tf_text[i]/float(len(tf_text))
        return tf_text

    def compute_idf(word, corpus):
        return math.log10(len(corpus)/sum([1.0 for i in corpus if word in i]))

    documents_list = []
    for text in corpus:
        tf_idf_dictionary = {}
        computed_tf = compute_tf(text)
        for word in computed_tf:
            tf_idf_dictionary[word] = computed_tf[word] * compute_idf(word, corpus)
        documents_list.append(tf_idf_dictionary)
    return documents_list

def nearestDocsByCosDist(sentences, query):
    index = IndexBuilder.load_reversed_index(sentences)
    normalized_query = IndexBuilder.normalize_words(query.split(" "))

    docs_set = set()
    weigths_vector = [1 for i in normalized_query]

    for word in normalized_query:
        if index.get(word):
            for sent_vec in index.get(word):
                docs_set.add(sent_vec)

    doc_tfidf_list_with_freq_map = compute_tfidf(sentences)

    mapa = {}  # key doc_number value - tfidf vector

    for doc in docs_set:
        doc_vector = []
        for word in normalized_query:
            if doc_tfidf_list_with_freq_map[doc].get(word):
                doc_vector.append(doc_tfidf_list_with_freq_map[doc].get(word))
            else:
                doc_vector.append(0)

        mapa.update({doc: doc_vector})

    cosine_distances = {}
    for doc_id, doc_vector in mapa.items():
        cosine_dist = spatial.distance.cosine(doc_vector, weigths_vector)
        cosine_distances.update({doc_id: cosine_dist})

    # Print 5 more relevant docs
    result=[]
    for doc in sorted(cosine_distances.items(), key=operator.itemgetter(1))[:10]:
        result.append(sentences[int(doc[0])])
    return result

def main():
    sentences = IndexBuilder.parse_xml("Klykov_data//space/output.xml")

    query = "модель позволит специалистам".decode("utf-8")

    for i in nearestDocsByCosDist(sentences, query):
        print (i)
