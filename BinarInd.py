def binaryDistance(sentences, query):
    doc_vector = []
    query_words = query.split(" ")
    for sent in sentences:
        sent_vec = []
        words = sent.split(" ")
        for q in query_words:
            check = 0
            for word in words:
                if word == q:
                    check = 1
                    break;
            sent_vec.append(check)
        doc_vector.append(sent_vec)
    return doc_vector