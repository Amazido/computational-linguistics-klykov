from Homework2.funcs import parse_xml, make_reversed_index, normalize_words, search_words_func


sentences = parse_xml("../output.xml")
reversed_index = make_reversed_index(sentences)

while 1:
    input_text = input("\nEnter a search query: ")
    search_words = normalize_words(input_text.split(" "))
    sents = search_words_func(reversed_index, search_words)

    if not sents:
        print("Nothing found")
        continue

    print("Query was found in sentences", sents)
    print("They are:")
    for i in sents:
        print(sentences[i])
