# coding=utf-8
import operator

import cosDistance
import IndexBuilder
from binaryIndex import binaryDistance


def main():
    sentences = IndexBuilder.parse_xml("Klykov_data//space/output.xml")

    query = "модель позволит специалистам".decode("utf-8")

    N = 10
    fisrt_result = cosDistance.nearestDocsByCosDist(sentences, query)
    for result in fisrt_result:
        print (result)

    input_var = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    # for i in fisrt_result:
    #     input_var.append(input("\nEnter 1 if document is relevant, 0 if not: "))
    #     print (i)

    R = 0
    for word_index in input_var:
        if word_index == 1:
            R += 1

    doc_vec = binaryDistance(fisrt_result, query)

    # print (doc_vec)

    n_vector = []
    query_words = query.split(" ")
    word_index = 0
    for q in query_words:
        n_vector.append(0)
        for doc in doc_vec:
            # print (doc[word_index])
            if doc[word_index] == 1:
                n_vector[word_index] += 1
        word_index += 1

    r_vector = []
    j = 0
    for q in query_words:
        r_vector.append(0)
        for i in input_var:
            if i == 1:
                r_vector[j] += doc_vec[i][j]
        j += 1

    a_vector = [z / R for z in r_vector]

    b_vector = []
    r_vector_index = 0
    for r_vectorchik in r_vector:
        res = (n_vector[r_vector_index] - r_vectorchik) / (N - R)
        b_vector.append(res)
        r_vector_index += 1

    term_weigts_vector = []
    a_vector_index = 0
    for a_vectorchik in a_vector:
        res = (a_vectorchik + 0.5) * (1 - b_vector[a_vector_index] + 0.5) / (
            (b_vector[a_vector_index] + 0.5) * (1 - a_vectorchik + 0.5))
        term_weigts_vector.append(res)
        a_vector_index += 1

    new_doc_weights_vector = []
    for doc in doc_vec:
        res = 0
        term_weight_index = 0
        for term_weight in term_weigts_vector:
            res += doc[term_weight_index] * term_weight
            term_weight_index += 1
        new_doc_weights_vector.append(res)

    print ("R=" + str(R) + "\n")
    print ("n_vector=" + str(n_vector) + "\n")
    print ("r_vector=" + str(r_vector) + "\n")
    print ("a_vector=" + str(a_vector) + "\n")
    print ("b_vector=" + str(b_vector) + "\n")
    print ("term_weigts_vector=" + str(term_weigts_vector) + "\n")
    print ("doc_vec=" + str(doc_vec) + "\n")

    answer_map = {}
    fisrt_result_counter = 0
    for weight in new_doc_weights_vector:
        answer_map.update({fisrt_result[fisrt_result_counter]: weight})
        fisrt_result_counter += 1

    print ("!!!!!NEW RESULT!!!!!")
    final_result = []
    for doc in sorted(answer_map.items(), key=operator.itemgetter(1))[:10]:
        final_result = answer_map.keys()
    for result in final_result:
        print (result)
    return answer_map;