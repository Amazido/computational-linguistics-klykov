import pymorphy2
from nltk.corpus import stopwords
from xml.etree import ElementTree


def parse_xml(path):
    sents = []
    tree = ElementTree.parse(path)
    root = tree.getroot()
    for node in root:
            for i in node:
                sents.append(i.text)
    return sents

def make_reversed_index(sents):
    index = {}
    i = 0
    for sent in sents:
        words = sent.split(" ")
        stop_words = stopwords.words('planet')
        words = [i for i in words if (i not in stop_words and i != " ")]
        words = normalize_words(words)
        update_index(index, words, i)
        i += 1
    return index

def normalize_words(words):
    morph = pymorphy2.MorphAnalyzer()
    norm_words = list(map(lambda word: morph.parse(word)[0].normal_form, words))
    return norm_words

def update_index(index, words, i):
    for word in words:
        if index.get(word):
            index.get(word).append(i)
        else:
            index.update({word: [i]})

def search_words_func(index, words):
    search_results = {}
    for word in words:
        found = index.get(word)
        if found:
            if not search_results:
                search_results = set(found)
            else:
                search_results = search_results.intersection(found)
    return search_results
