# coding=utf-8
import os

import json

import pymorphy2
from nltk.corpus import stopwords
from lxml import etree


def load_reversed_index(sentences):
    if os.path.exists('index_backup.json'):
        print("Found saved reversed index. Loading from file...")
        with open('index_backup.json', 'r') as fp:
            reversed_index = json.load(fp)
    else:
        print("Saved reversed index not found. Building a new one...")
        # File does not exist, create an index and save it to file
        reversed_index = build_reversed_index(sentences)
        with open('index_backup.json', 'w') as fp:
            json.dump(reversed_index, fp, sort_keys=True, indent=4)

    return reversed_index

def parse_xml(filePath):
    sentences = []
    tree = etree.parse(filePath)
    doc = tree.getroot()
    for node in doc:
        sentences.append(node.text)
    return sentences


def build_reversed_index(sentences):
    print("INDEX BUILDING START...")
    i = 0
    index = {}
    for sent in sentences:
        words = sent.split(" ")
        stop_words = stopwords.words('russian')
        words = [j for j in words if (j not in stop_words and j != " " and j != "")]
        words = normalize_words(words)
        update_index(index, words, i)
        i += 1
    print("INDEX FINISHED")
    return index


def normalize_words(words):
    morph = pymorphy2.MorphAnalyzer()
    words_to_ret = list(map(lambda word: morph.parse(word)[0].normal_form, words))
    return words_to_ret


def update_index(index, words, i):
    for word in words:
        if index.get(word):
            index.get(word).append(i)
        else:
            index.update({word: [i]})

